
const express = require('express');
const path = require('path');
require('dotenv').config();
const { generateJWT, checkJWT } = require('./helpers/jwt');
const app = express();
app.set("trust proxy", true)
//lectura y parseo del body
app.use(express.json())
// Nodejs server
const server = require('http').createServer(app);
module.exports.io = require('socket.io')(server);
require('./sockets/socket');

const publicPath = path.resolve( __dirname, 'public');
app.use( express.static(publicPath))

app.get('/generar-token', async (req, res) => {
    try {
       
        const token = await generateJWT(req.body);
        res.json({ token });
    } catch (error) {
        res.status(500).json({ error: 'Hubo un error al generar el token' });
    }
});
 


server.listen(process.env.PORT, (error)=>  {

    if(error) throw new Error(error);

    console.log('Server on port !!: ', process.env.PORT);

});
