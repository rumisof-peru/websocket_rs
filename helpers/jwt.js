
const jwt = require('jsonwebtoken')

const generateJWT = (uid) => {

    return new Promise((resolve, reject) => {

        const payload = { uid }

        jwt.sign(payload, process.env.JWT_SECRET_KEY, {
            // expiresIn: '24h'
        }, (error, token) => {
            if (error) {
                reject('the JWT could not be generate')
            }
            else {
                resolve(token)
            }
        })
    })
}

const checkJWT = (token = '') => {

    try {

        const response = jwt.verify(token, process.env.JWT_SECRET_KEY)

        return [true, response.uid];

    } catch (error) {

        return [false, null];
    }
}

module.exports = {
    generateJWT,
    checkJWT
}