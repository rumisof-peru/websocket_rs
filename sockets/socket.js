const { JSON } = require('sequelize');
const { checkJWT } = require('../helpers/jwt');
const { io } = require('../index');
const Database = require('./db');

// Usaremos un objeto para mantener un registro de canales dinámicos.
const channels = {};

io.on('connection', client => {

    console.log('nueva conexion..');

    const [valid, data] = checkJWT(client.handshake.headers['x-token']);

    if (!valid) {
        console.log("TOKEN INVALIDO... !!");
        return client.disconnect();
    } else {

        const { userid, locationid, username, locationname, application,role } = client.handshake.headers;

        if (application == "ninja-pos") {
            const clientData = {
                id: client.id,
                userId: userid,
                userName: username,
                role: role,
                locationId: locationid,
                locationName: locationname,
                status: 'connected',
            };

            // Crear un canal basado en locationId si no existe.
            if (!channels[clientData.locationId]) {
                channels[clientData.locationId] = [];
            }

            // Si existe el usuario, eliminarlo para crear una nueva lista del canal.
            if (channels[clientData.locationId].some(user => user.userId === clientData.userId)) {
                channels[clientData.locationId] = channels[clientData.locationId].filter(user => user.userId !== clientData.userId);
            }

            // Verificar si el usuario ya está registrado en el canal.
            if (!channels[clientData.locationId].some(user => user.userId === clientData.userId)) {
                client.join(clientData.locationId);
                channels[clientData.locationId].push(clientData);

                console.log(`${clientData.locationId} ${clientData.id} Usuario ${clientData.userName} se unió al canal ${clientData.locationName} `);

                // Enviar sus credenciales al Usuario conectado 
                io.to(clientData.id).emit('userJoined', clientData);

               // console.log(channels);

            } else {
                console.log("El usuario ya existe...");
                io.to(clientData.id).emit('userJoined', clientData);
            }
        }

        client.on('sendMessage', ({ channel, message }) => {
            console.log(message);
            io.to(channel).emit('message', message);
        });

        client.on('privateMessage', ({ to, message }) => {
            // Agregada la validación de la existencia del canal
            const userChannel = channels[clientData.locationId];
            if (userChannel) {
                const targetUser = userChannel.find(user => user.userId === to);
                if (targetUser) {
                    io.to(targetUser.id).emit('privateMessage', message);
                } else {
                    client.emit('privateMessage', 'El usuario destino no se encuentra en el canal');
                }
            } else {
                client.emit('privateMessage', 'El canal no existe o está vacío');
            }
        });

        client.on('privateSendQR', ({ to, canal, intentos, qr }) => {
            console.log(`[privateSendQR] Evento recibido - Destino: ${to}, Canal: ${canal}, Intentos: ${intentos}, QR: ${qr ? 'Sí' : 'No'}`);
        
            // Validación de la existencia del canal
            const userChannel = channels[canal];
        
            if (userChannel) {
                console.log(`[privateSendQR] Canal encontrado: ${canal}, Usuarios en el canal: ${userChannel.length}`);
        
                const targetUser = userChannel.find(user => user.id === to);
        
                if (targetUser) {
                    console.log(`[privateSendQR] Usuario encontrado en el canal (${canal}): ${targetUser.id}. Enviando QR...`);
                    io.to(targetUser.id).emit('privateMessageQR', { qr });
                } else {
                    console.warn(`[privateSendQR] Usuario destino (${to}) no encontrado en el canal (${canal})`);
                    client.emit('privateMessage', 'El usuario destino no se encuentra en el canal');
                }
            } else {
                console.error(`[privateSendQR] Error: El canal (${canal}) no existe o está vacío.`);
                client.emit('privateMessage', 'El canal no existe o está vacío');
            }
        });
        

        client.on('sessionQrOk', ({ to, canal, msj }) => {
            console.log("SESION INICIADA :", to, canal, msj);
            // Verificación añadida para asegurar que el canal existe
            const userChannel = channels[canal];
            if (userChannel) {
                const targetUser = userChannel.find(user => user.id === to);
                if (targetUser) {
                    io.to(targetUser.id).emit('privateStatusSessionQR', { msj: msj });
                } else {
                    console.log('privateStatusSessionQR', 'El usuario destino no se encuentra en el canal', to, canal);
                }
            } else {
                console.log('privateStatusSessionQR', 'El canal no existe o está vacío', to, canal);
            }
        });

        client.on('disconnectedQr', ({ to, canal, msj }) => {
            console.log("SESION FALLIDA :", to, canal, msj);
            // Verificación añadida para asegurar que el canal existe
            const userChannel = channels[canal];
            if (userChannel) {
                const targetUser = userChannel.find(user => user.id === to);
                if (targetUser) {
                    io.to(targetUser.id).emit('disconnectedQr', { msj: msj });
                } else {
                    console.log('disconnectedQr', 'El usuario destino no se encuentra en el canal', to, canal);
                }
            } else {
                console.log('disconnectedQr', 'El canal no existe o está vacío', to, canal);
            }
        });
        
        client.on('sendMessageToAdmin_deleteItem', async ({ data, mosoId, locationId }) => {
            console.log(`Enviando mensaje a todos los ADMINs en el canal ${locationId}: ${mosoId}`);
           

            // const db = new Database("rumisof_blanca");
            // await db.connect();
            // try {
            //   const selectQuery = 'SELECT * FROM printer WHERE printer_id = :printer_id';
            //   const result = await db.select(selectQuery, { printer_id: "0" });
            //   console.log('Resultado del SELECT:', result);
            // } catch (error) {
            //   console.error('Error en SELECT:', error);
            // } finally {
            //   await db.close();
            // }
            
           
            const userChannel = channels[locationId];
            if (userChannel) {
                userChannel.forEach(user => {
                    if (user.role === 'ADMIN') {
                        const dataModel = {
                            data: data,
                            mosoId: mosoId,
                            locationId: locationId,
                            message: `El usuario ${data.userName} de la mesa ${data.tableName}, intenta eliminar un producto que está en preparación`,
                            timestamp: new Date().toISOString(),
                        };
                        io.to(user.id).emit('adminMessageDeleteItem', { data: dataModel });
                        console.log("enviado... a ",user.id,user.userName);
                    }
                });
            } else {
                client.emit('adminMessage', 'El canal no existe o está vacío');
            }
        });

        client.on('sendConfirmDeleteItem_deleteItem', async ({ data, adminId, status }) => {
            const { ...dataModel } = data.data;  // Se desestructura correctamente el objeto de data
        
            console.log(dataModel.data);
            console.log(dataModel.locationId);
            console.log(dataModel.message);
            console.log(dataModel.timestamp);
            console.log(dataModel.mosoId);
            console.log(adminId);
            console.log(status);
        
            const userChannel = channels[dataModel.locationId];
            if (userChannel) {
                userChannel.forEach(user => {
                    if (user.role === 'MOSO') {
                        // Renombrar esta variable a algo diferente
                        const updateModel = {
                            data: dataModel.data,
                            mosoId: dataModel.mosoId,
                            adminId: adminId,
                            locationId: dataModel.locationId,
                            message: (status) ? 'El administrador aceptó la solicitud' : 'El administrador rechazó la solicitud',
                            timestamp: new Date().toISOString(),
                            status: status,
                        };
                        io.to(user.id).emit('deleteItemStatusAdminToMoso', { data: updateModel });
                        console.log("Enviado... a ", user.id, user.userName);
                    }
                });
            } else {
                client.emit('adminMessage', 'El canal no existe o está vacío');
            }
        });
        

        // Añadido el evento 'disconnect' para manejar la desconexión del cliente
        client.on('disconnect', () => {
            // Eliminar el usuario de todos los canales al desconectarse
            for (const channel in channels) {
                channels[channel] = channels[channel].filter(user => user.id !== client.id);
                if (channels[channel].length === 0) {
                    delete channels[channel];
                }
            }
            console.log('Cliente desconectado:', client.id);
        });
    }
});
