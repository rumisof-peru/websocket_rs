// database.js
const { Sequelize, DataTypes, QueryTypes } = require('sequelize');

class Database {
  constructor(databaseName) {    
    this.sequelize = new Sequelize(databaseName, process.env.DB_USER, process.env.DB_PASS, {
      host: process.env.DB_HOST,
      dialect: 'mysql',
      logging: false, // Desactivar el logging para evitar información de consulta en consola
    });
  }

  async connect() {
    try {
      await this.sequelize.authenticate();
      console.log('Conexión establecida con éxito a la base de datos.');
    } catch (error) {
      console.error('No se pudo conectar a la base de datos:', error);
    }
  }

  async close() {
    try {
      await this.sequelize.close();
      console.log('Conexión cerrada con éxito.');
    } catch (error) {
      console.error('Error al cerrar la conexión a la base de datos:', error);
    }
  }

  async select(query, replacements = {}) {
    try {
      const result = await this.sequelize.query(query, {
        replacements: replacements,
        type: QueryTypes.SELECT,
      });
      return result;
    } catch (error) {
      console.error('Error en SELECT:', error);
      throw error;
    }
  }

  async update(query, replacements = {}) {
    try {
      const result = await this.sequelize.query(query, {
        replacements: replacements,
        type: QueryTypes.UPDATE,
      });
      return result;
    } catch (error) {
      console.error('Error en UPDATE:', error);
      throw error;
    }
  }
}

module.exports = Database;
